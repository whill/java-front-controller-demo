package com.workshop.interfaces;

import com.workshop.implementation.JSPView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by winfieldhill on 3/18/15.
 * ...
 */
public interface Controller {

    public JSPView execute(HttpServletRequest request, HttpServletResponse response) throws Exception;
}
