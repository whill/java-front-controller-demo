package com.workshop.implementation.servlets;

import com.workshop.implementation.ControllerConfig;
import com.workshop.implementation.JSPView;
import com.workshop.implementation.WebConfig;
import com.workshop.interfaces.Controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by winfieldhill on 3/18/15.
 * ...
 */
public class WorkshopServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static final String MAPPING_FILE = "mapping";

    private ServletContext context;
    private WebConfig webConfig = null;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        this.context = config.getServletContext();
        String mappingFile;
        String controllerProperties = config.getInitParameter(MAPPING_FILE);
        mappingFile = this.context.getRealPath(controllerProperties);
        this.webConfig = new WebConfig(mappingFile);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doProcess(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doProcess(request, response);
    }

    private void doProcess(HttpServletRequest request, HttpServletResponse response) {
        final String servletPath = request.getServletPath();
        final String actionPath = servletPath.substring(1,servletPath.lastIndexOf("."));
        final Map controllers = webConfig.getControllers();
        final ControllerConfig ctrConfig = (ControllerConfig) controllers.get(actionPath);

        JSPView jspView = null;

        if (ctrConfig != null) {
            String actionUrl = ctrConfig.getAction();
            if (actionPath.equals(actionUrl)) {
                try {
                    final Controller controller = ctrConfig.invokeController();
                    jspView = controller.execute(request, response);
                    prepareModelData(request, jspView);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                dispatchRequestToView(jspView, webConfig, request, response);
            }
        } else throw new UnsupportedOperationException(
                "action " + actionPath + " is not found in mapping file."
            );
    }

    private void dispatchRequestToView(JSPView jspView, WebConfig webConfig,
                                       HttpServletRequest request, HttpServletResponse response) {
        try {
            String forward = jspView.getForward();
            final RequestDispatcher rd = context.getRequestDispatcher(forward);
            rd.forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void prepareModelData(HttpServletRequest request, JSPView jspView) {
        final Map model = jspView.getModel();
        if (model != null) {
            Set es = model.entrySet();
            Iterator it = es.iterator();
            while (it.hasNext()) {
                String key = (String)it.next();
                Object value = model.get(key);
                request.setAttribute(key, value);
            }
        }
    }
}
