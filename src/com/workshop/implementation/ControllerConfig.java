package com.workshop.implementation;

import static java.lang.Class.forName;
import com.workshop.interfaces.Controller;

/**
 * Created by winfieldhill on 3/18/15.
 * ...
 */
public class ControllerConfig {

    private String action;

    private String controllerClass;

    public ControllerConfig(String action, String controllerClass) {
        //super();
        setAction(action);
        setControllerClass(controllerClass);
    }

    public Controller invokeController() {
        Controller controller = null;

        if (controllerClass != null && !controllerClass.isEmpty()) {
            try {
                controller = (Controller)forName(controllerClass).newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return controller;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getControllerClass() {
        return controllerClass;
    }

    public void setControllerClass(String controllerClass) {
        this.controllerClass = controllerClass;
    }
}
