package com.workshop.implementation;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * Created by winfieldhill on 3/18/15.
 * ...
 */
public final class WebConfig {

    private static final String MAPPING = "mappings.properties";
    private Map<String, ControllerConfig> controllers = null;

    public WebConfig(String mappingFile) {//throws IOException{
        File mapping = new File(mappingFile);

        //System.out.println(mapping.getPath());

        if (mapping.isFile() && mapping.exists()) {
            String xmlFileName = mapping.getName();
            if (!xmlFileName.equals(MAPPING)) {
                throw new UnsupportedOperationException(
                    "MAPPING file name should be " + MAPPING
                );
            }
            controllers = new HashMap<>();
            readMappingFile(mapping);
        } else {
            System.err.println("Not a mapping file or the file doesn't exist." + mapping.getPath());
        }
    }

    private void readMappingFile(File propertyFile) {
        Properties props = new Properties();

        try {
            props.load(new FileInputStream(propertyFile));
            Set keys = props.keySet();
            Iterator keyIterator = keys.iterator();

            while (keyIterator.hasNext()) {
                String key = (String)keyIterator.next();
                String className = (String)props.get(key);
                ControllerConfig controllerConfig = new ControllerConfig(key, className);
                addControllerConfig(key, controllerConfig);
            }
        }
        catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private void addControllerConfig(String actionURL, ControllerConfig controllerConfig) {

        if (!controllers.containsKey(actionURL))
            controllers.put(actionURL, controllerConfig);
        else throw new UnsupportedOperationException(
            "This action <" + actionURL + "> already exists. Only one controller per action."
        );
    }

    public Map<String, ControllerConfig> getControllers() {
        return controllers;
    }
}
