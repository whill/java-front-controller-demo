package com.workshop.implementation;

import java.util.Map;

/**
 * Created by winfieldhill on 3/18/15.
 * ...
 */
public class JSPView {

    private Map model;
    private String forward;

    public JSPView(String forward) {
        this.setForward(forward);
    }

    public JSPView(String forward, Map data) {
        this.setForward(forward);
        if (data != null) {
            this.model = data;
        }
    }

    public void clean() {
        this.model = null;
    }

    public Map getModel() {
        return model;
    }

    public void setModel(Map model) {
        this.model = model;
    }

    public String getForward() {
        return forward;
    }

    public void setForward(String forward) {
        this.forward = forward;
    }
}
