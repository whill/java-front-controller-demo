package com.workshop.implementation.controllers;

import com.workshop.implementation.JSPView;
import com.workshop.interfaces.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by winfieldhill on 3/18/15.
 * ...
 */
public class LoginController implements Controller {

    @Override
    public JSPView execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        System.out.println("Using the LoginController");
        return new JSPView("/login.jsp");
        //return new JSPView("/login.jsp", some map of request params to use in JSP...);
    }
}
